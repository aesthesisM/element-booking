FROM openjdk:11
ADD target/booking-1.0.0.jar booking.jar
ENTRYPOINT ["java","-jar","booking.jar"]
