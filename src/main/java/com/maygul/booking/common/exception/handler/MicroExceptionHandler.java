package com.maygul.booking.common.exception.handler;

import com.maygul.booking.common.exception.constant.MicroErrorType;
import com.maygul.booking.common.exception.data.ExceptionData;
import com.maygul.booking.common.exception.data.ValidationErrorData;
import com.maygul.booking.common.exception.type.MicroException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class MicroExceptionHandler extends ResponseEntityExceptionHandler {
    
    private final Logger log = LoggerFactory.getLogger(MicroExceptionHandler.class);
    
    @Value("${spring.application.name}")
    public String applicationName;
    
    @Autowired
    private MessageSource messageSource;
    
    private static final String GENERIC_ERROR_MESSAGE_KEY = "com.maygul.booking.generic.exception.message";
    
    public MicroExceptionHandler() {
    }
    
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = MicroException.class)
    public ExceptionData handleMicroException(MicroException ex, Locale locale) {
        return errorResponse(ex, ex.getExceptionData(), locale);
    }
    
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = UndeclaredThrowableException.class)
    public ExceptionData handleUndeclaredThrowableException(UndeclaredThrowableException ex, Locale locale) {
        if (ex.getCause() instanceof MicroException) {
            MicroException exception = (MicroException) ex.getCause();
            return errorResponse(ex, exception.getExceptionData(), locale);
        }
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.INTERNAL_ERROR.getCode(), ex.getMessage());
        return errorResponse(ex, exceptionData, locale);
    }
    
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public ExceptionData handleException(Exception ex, Locale locale) {
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.INTERNAL_ERROR.getCode(), ex.getMessage());
        return errorResponse(ex, exceptionData, locale);
    }
    
    private ExceptionData errorResponse(Exception ex, ExceptionData exceptionData, Locale locale) {
        log.error("Exception handler caught an error:" + ExceptionUtils.getStackTrace(ex));
        populateExceptionDataWithServiceInformation(exceptionData, locale, ex);
        return exceptionData;
    }
    
    private void populateExceptionDataWithServiceInformation(ExceptionData exceptionData, Locale locale, Exception ex) {
        if (exceptionData != null) {
            if (StringUtils.isEmpty(exceptionData.getApplicationName())) {
                exceptionData.setApplicationName(applicationName);
            }
            if (!StringUtils.isEmpty(exceptionData.getErrorMessage())) {
                String formattedMessage = null;
                try {
                    formattedMessage = messageSource.getMessage(exceptionData.getErrorMessage(), null, locale);
                } catch (NoSuchMessageException ex2) {
                }
                if (formattedMessage == null) {
                    formattedMessage = messageSource.getMessage(GENERIC_ERROR_MESSAGE_KEY, null, locale);
                }
                exceptionData.setErrorMessage(formattedMessage != null ? formattedMessage : exceptionData.getErrorMessage());
            }
        }
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.VALIDATION_ERROR.getCode(), MicroErrorType.VALIDATION_ERROR.getMessage());
        List<ValidationErrorData> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            ValidationErrorData fieldError = new ValidationErrorData(error.getField(), error.getDefaultMessage());
            errors.add(fieldError);
        }
        exceptionData.setErrors(errors);
        errorResponse(ex, exceptionData, request.getLocale());
        return handleExceptionInternal(ex, exceptionData, headers, HttpStatus.BAD_REQUEST, request);
    }
    
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
                                                                         HttpHeaders headers,
                                                                         HttpStatus status,
                                                                         WebRequest request) {
        
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t).append(" "));
        
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.METHOD_NOT_ALLOWED.getCode(), builder.toString());
        errorResponse(ex, exceptionData, request.getLocale());
        
        return handleExceptionInternal(ex, exceptionData, headers, HttpStatus.METHOD_NOT_ALLOWED, request);
        
    }
    
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
                                                                   HttpHeaders headers,
                                                                   HttpStatus status,
                                                                   WebRequest request) {
        
        String errorMessage = new StringBuilder("No handler found for ").append(ex.getHttpMethod()).append(" ").append(ex.getRequestURL()).toString();
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.HANDLER_NOT_FOUND.getCode(), errorMessage);
        
        errorResponse(ex, exceptionData, request.getLocale());
        return handleExceptionInternal(ex, exceptionData, headers, HttpStatus.NOT_FOUND, request);
    }
    
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        String errorMessage = new StringBuilder("Invalid request: ").append(ex.getMessage()).toString();
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.INVALID_REQUEST.getCode(), errorMessage);
        errorResponse(ex, exceptionData, request.getLocale());
        
        return handleExceptionInternal(ex, exceptionData, headers, HttpStatus.BAD_REQUEST, request);
    }
}

