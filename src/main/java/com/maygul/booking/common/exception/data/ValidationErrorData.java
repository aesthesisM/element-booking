package com.maygul.booking.common.exception.data;

import java.io.Serializable;

public class ValidationErrorData implements Serializable {

    private String field;
    private String message;

    public ValidationErrorData() {
    }

    public ValidationErrorData(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
