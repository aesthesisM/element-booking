package com.maygul.booking.common.exception.data;

import java.io.Serializable;
import java.util.List;

public class ExceptionData implements Serializable {
    
    private String applicationName;
    private Long errorCode;
    private String errorMessage;
    
    private List<ValidationErrorData> errors;
    
    public ExceptionData() {
    }
    
    public ExceptionData(Long errorCode) {
        this.errorCode = errorCode;
    }
    
    public ExceptionData(Long errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
    
    public ExceptionData(String applicationName, Long errorCode, String errorMessage) {
        this.applicationName = applicationName;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
    
    public String getApplicationName() {
        return applicationName;
    }
    
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }
    
    
    public String getErrorMessage() {
        return errorMessage;
    }
    
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public List<ValidationErrorData> getErrors() {
        return errors;
    }
    
    public void setErrors(List<ValidationErrorData> errors) {
        this.errors = errors;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[").
            append("applicationName:").
            append(applicationName).
            append(",").
            append("errorCode:").
            append(errorCode).
            append(",").
            append("errorMessage:").
            append(errorMessage).
            append("]");
        return builder.toString();
    }
}
