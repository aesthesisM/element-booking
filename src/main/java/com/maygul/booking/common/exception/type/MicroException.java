package com.maygul.booking.common.exception.type;


import com.maygul.booking.common.exception.data.ExceptionData;

public class MicroException extends Exception {
    
    private ExceptionData data;
    
    public MicroException(ExceptionData data) {
        super(data.getErrorMessage());
        this.data = data;
    }
    
    public MicroException(Long errorCode, String message) {
        this(new ExceptionData(errorCode, message));
    }
    
    public ExceptionData getExceptionData() {
        return data;
    }
}
