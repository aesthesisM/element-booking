package com.maygul.booking.exception;

import com.maygul.booking.common.exception.type.MicroException;

public class BookingNotFoundException extends MicroException {
    public BookingNotFoundException() {
        super(BookingExceptionType.BOOKING_NOT_FOUND.getCode(), BookingExceptionType.BOOKING_NOT_FOUND.getMessage());
    }
}
