package com.maygul.booking.exception;

import com.maygul.booking.common.exception.type.MicroException;

public class HikerAgeIsNotAppropriateException extends MicroException {
    public HikerAgeIsNotAppropriateException() {
        super(BookingExceptionType.HIKER_AGE_NOT_APPROPRIATE.getCode(), "com.maygul.booking.exception.hiker.age.not.appropriate");
    }
}
