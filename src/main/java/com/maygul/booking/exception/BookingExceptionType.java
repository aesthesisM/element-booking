package com.maygul.booking.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BookingExceptionType {
    BOOKING_NOT_FOUND(1L, "com.maygul.booking.exception.booking.not.found"),
    TRAIL_NOT_FOUND(2L,"com.maygul.booking.exception.trail.not.found"),
    HIKER_AGE_NOT_APPROPRIATE(3L,"com.maygul.booking.exception.hiker.age.not.appropriate");
    
    private Long code;
    private String message;
}
