package com.maygul.booking.exception;

import com.maygul.booking.common.exception.type.MicroException;

public class TrailNotFoundException extends MicroException {
    public TrailNotFoundException() {
        super(BookingExceptionType.TRAIL_NOT_FOUND.getCode(), BookingExceptionType.TRAIL_NOT_FOUND.getMessage());
    }
}
