package com.maygul.booking.service;

import com.maygul.booking.exception.TrailNotFoundException;
import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.persistence.specification.criteria.TrailSearchCriteria;
import com.maygul.booking.service.dto.TrailDto;
import com.maygul.booking.service.dto.SearchPageTrailDto;

public interface TrailService {
    
    Trail getHikingEntityById(Long id) throws TrailNotFoundException;
    
    SearchPageTrailDto search(Integer pageNumber, Integer pageSize, TrailSearchCriteria criteria);
    
    TrailDto getTrailDtoById(Long id) throws TrailNotFoundException;
}
