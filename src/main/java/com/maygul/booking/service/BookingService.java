package com.maygul.booking.service;

import com.maygul.booking.api.request.CreateBookingRequest;
import com.maygul.booking.exception.BookingNotFoundException;
import com.maygul.booking.exception.HikerAgeIsNotAppropriateException;
import com.maygul.booking.exception.TrailNotFoundException;
import com.maygul.booking.persistence.specification.criteria.BookingSearchCriteria;
import com.maygul.booking.service.dto.BookingDto;
import com.maygul.booking.service.dto.SearchPageBookingDto;

public interface BookingService {
    SearchPageBookingDto search(Integer pageNumber, Integer pageSize, BookingSearchCriteria criteria);
    
    BookingDto getBookingDtoById(Long id) throws BookingNotFoundException;
    
    BookingDto cancelBookingById(Long id) throws BookingNotFoundException;
    
    BookingDto save(CreateBookingRequest request) throws TrailNotFoundException, HikerAgeIsNotAppropriateException;
}
