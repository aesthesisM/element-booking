package com.maygul.booking.service.impl;

import com.maygul.booking.api.request.CreateBookingRequest;
import com.maygul.booking.exception.BookingNotFoundException;
import com.maygul.booking.exception.HikerAgeIsNotAppropriateException;
import com.maygul.booking.exception.TrailNotFoundException;
import com.maygul.booking.mapper.BookingMapper;
import com.maygul.booking.persistence.entity.Booking;
import com.maygul.booking.persistence.entity.BookingStatus;
import com.maygul.booking.persistence.entity.Hiker;
import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.persistence.repository.BookingRepository;
import com.maygul.booking.persistence.repository.HikerRepository;
import com.maygul.booking.persistence.repository.TrailRepository;
import com.maygul.booking.persistence.specification.BookingSpecification;
import com.maygul.booking.persistence.specification.criteria.BookingSearchCriteria;
import com.maygul.booking.service.BookingService;
import com.maygul.booking.service.dto.BookingDto;
import com.maygul.booking.service.dto.HikerDto;
import com.maygul.booking.service.dto.SearchPageBookingDto;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class BookingServiceImpl implements BookingService {
    private TrailRepository trailRepository;
    private HikerRepository hikerRepository;
    private BookingRepository bookingRepository;
    private BookingMapper bookingMapper;
    
    @Override
    public SearchPageBookingDto search(Integer pageNumber, Integer pageSize, BookingSearchCriteria criteria) {
        Specification<Booking> specification = BookingSpecification.findBySearchCriteria(criteria);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        
        Page<Booking> pageResult = bookingRepository.findAll(specification, pageable);
        
        List<Booking> result = pageResult.getContent();
        Long totalCount = pageResult.getTotalElements();
        
        SearchPageBookingDto response = new SearchPageBookingDto(bookingMapper.entityListToSearchDtoList(result), totalCount);
        return response;
    }
    
    @Override
    @Transactional
    public BookingDto getBookingDtoById(Long id) throws BookingNotFoundException {
        Booking entity = bookingRepository.findById(id).orElseThrow(BookingNotFoundException::new);
        BookingDto dto = bookingMapper.entityToDto(entity);
        return dto;
    }
    
    @Override
    public BookingDto cancelBookingById(Long id) throws BookingNotFoundException {
        Booking entity = bookingRepository.findById(id).orElseThrow(BookingNotFoundException::new);
        
        entity.setStatus(BookingStatus.CANCELED);
        
        bookingRepository.save(entity);
        
        BookingDto dto = bookingMapper.entityToDto(entity);
        return dto;
    }
    
    @Override
    @Transactional
    public BookingDto save(CreateBookingRequest request) throws TrailNotFoundException, HikerAgeIsNotAppropriateException {
        Trail trailEntity = trailRepository.findById(request.getTrailId()).orElseThrow(TrailNotFoundException::new);
        Double totalUserCost = 0d;
        List<Hiker> hikers = new ArrayList<>();
        Booking booking = new Booking();
        for (int i = 0; i < request.getHikers().size(); i++) {
            HikerDto dto = request.getHikers().get(i);
            
            if (dto.getAge() < trailEntity.getMinimumAge() || dto.getAge() > trailEntity.getMaximumAge()) {
                throw new HikerAgeIsNotAppropriateException();
            }
            
            Hiker hikerEntity = new Hiker();
            hikerEntity.setAge(dto.getAge());
            hikerEntity.setUsername(dto.getUsername());
            hikerEntity.setBooking(booking);
            hikers.add(hikerEntity);
            totalUserCost += trailEntity.getUnitPrice();
        }
        
        booking.setStatus(BookingStatus.APPROVED);
        booking.setTrail(trailEntity);
        booking.setHikers(hikers);
        booking.setHikingDate(request.getHikingDate());
        booking.setTotalCostForHikers(totalUserCost);
        bookingRepository.save(booking);
        hikerRepository.saveAll(hikers);
        
        BookingDto dto = bookingMapper.entityToDto(booking);
        
        return dto;
    }
}
