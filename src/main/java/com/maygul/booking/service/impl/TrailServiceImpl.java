package com.maygul.booking.service.impl;

import com.maygul.booking.exception.TrailNotFoundException;
import com.maygul.booking.mapper.TrailMapper;
import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.persistence.repository.TrailRepository;
import com.maygul.booking.persistence.specification.TrailSpecification;
import com.maygul.booking.persistence.specification.criteria.TrailSearchCriteria;
import com.maygul.booking.service.TrailService;
import com.maygul.booking.service.dto.SearchPageTrailDto;
import com.maygul.booking.service.dto.TrailDto;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TrailServiceImpl implements TrailService {
    
    private TrailRepository trailRepository;
    private TrailMapper trailMapper;
    
    @Override
    public Trail getHikingEntityById(Long id) throws TrailNotFoundException {
        Trail entity = trailRepository.findById(id).orElseThrow(TrailNotFoundException::new);
        return entity;
    }
    
    @Override
    public SearchPageTrailDto search(Integer pageNumber, Integer pageSize, TrailSearchCriteria criteria) {
        
        Specification<Trail> specification = TrailSpecification.findBySearchCriteria(criteria);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        
        Page<Trail> pageResponse = trailRepository.findAll(specification, pageable);
        List<Trail> result = pageResponse.getContent();
        Long totalCount = pageResponse.getTotalElements();
        
        SearchPageTrailDto searchPageTrailDto = new SearchPageTrailDto(trailMapper.entityListToDtoList(result), totalCount);
        return searchPageTrailDto;
    }
    
    @Override
    public TrailDto getTrailDtoById(Long id) throws TrailNotFoundException {
        Trail entity = trailRepository.findById(id).orElseThrow(TrailNotFoundException::new);
        
        TrailDto dto = trailMapper.entityToDto(entity);
        return dto;
    }
}
