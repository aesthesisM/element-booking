package com.maygul.booking.service.dto;

import com.maygul.booking.common.service.dto.BaseSearchPageDto;
import java.util.List;

public class SearchPageTrailDto extends BaseSearchPageDto<SearchTrailDto> {
    public SearchPageTrailDto(List<SearchTrailDto> pageResponse, Long totalCount) {
        super(pageResponse, totalCount);
    }
}
