package com.maygul.booking.service.dto;

import java.time.LocalTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchTrailDto {
    private Long id;
    private String name;
    private LocalTime startAt;
    private LocalTime endAt;
    private Long minimumAge;
    private Long maximumAge;
    private Double unitPrice;
}
