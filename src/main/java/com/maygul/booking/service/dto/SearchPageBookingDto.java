package com.maygul.booking.service.dto;

import com.maygul.booking.common.service.dto.BaseSearchPageDto;
import java.util.List;

public class SearchPageBookingDto extends BaseSearchPageDto<SearchBookingDto> {
    public SearchPageBookingDto(List<SearchBookingDto> pageResponse, Long totalCount) {
        super(pageResponse, totalCount);
    }
}
