package com.maygul.booking.service.dto;

import com.maygul.booking.persistence.entity.BookingStatus;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingDto {
    private Long id;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private LocalDate hikingDate;
    private BookingStatus status;
    private Long trailId;
    private Double totalCostForHikers;
    private List<HikerDto> hikers;
}
