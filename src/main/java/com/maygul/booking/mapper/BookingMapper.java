package com.maygul.booking.mapper;

import com.maygul.booking.persistence.entity.Booking;
import com.maygul.booking.service.dto.BookingDto;
import com.maygul.booking.service.dto.SearchBookingDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {HikerMapper.class})
public interface BookingMapper {
    @Mappings({
        @Mapping(source = "trail.id", target = "trailId")
    })
    BookingDto entityToDto(Booking entity);
    
    @Mappings({
        @Mapping(source = "trail.id", target = "trailId")
    })
    SearchBookingDto entityToSearchDto(Booking entity);
    
    List<SearchBookingDto> entityListToSearchDtoList(List<Booking> entityList);
    
}
