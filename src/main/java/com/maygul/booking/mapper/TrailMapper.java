package com.maygul.booking.mapper;

import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.service.dto.SearchTrailDto;
import com.maygul.booking.service.dto.TrailDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TrailMapper {
    
    TrailDto entityToDto(Trail entity);
    
    SearchTrailDto entityToSearchDto(Trail entity);
    
    List<SearchTrailDto> entityListToDtoList(List<Trail> entityList);
    
}
