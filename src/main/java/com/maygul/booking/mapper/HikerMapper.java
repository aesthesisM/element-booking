package com.maygul.booking.mapper;

import com.maygul.booking.persistence.entity.Hiker;
import com.maygul.booking.service.dto.HikerDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HikerMapper {
    
    HikerDto entityToDto(Hiker entity);
    
    List<HikerDto> entityListToDtoList(List<Hiker> entityList);
    
}
