package com.maygul.booking.api;

import com.maygul.booking.api.request.CreateBookingRequest;
import com.maygul.booking.exception.BookingNotFoundException;
import com.maygul.booking.exception.HikerAgeIsNotAppropriateException;
import com.maygul.booking.exception.TrailNotFoundException;
import com.maygul.booking.persistence.specification.criteria.BookingSearchCriteria;
import com.maygul.booking.service.BookingService;
import com.maygul.booking.service.dto.BookingDto;
import com.maygul.booking.service.dto.SearchPageBookingDto;
import io.swagger.annotations.Api;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("Booking controller")
@RestController
@RequestMapping("/api/booking")
@AllArgsConstructor
public class BookingController {
    
    private BookingService bookingService;
    
    @GetMapping("/search")
    public ResponseEntity<SearchPageBookingDto> search(@RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
                                                       @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                       @RequestParam(name = "trailId", required = false) Long trailId,
                                                       @RequestParam(name = "hikingDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate hikingDate,
                                                       @RequestParam(name = "beginDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime beginDate,
                                                       @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate) {
        BookingSearchCriteria criteria = new BookingSearchCriteria(trailId, hikingDate, beginDate, endDate);
        SearchPageBookingDto searchDto = bookingService.search(pageNumber, pageSize, criteria);
        return new ResponseEntity<>(searchDto, HttpStatus.OK);
    }
    
    @PutMapping
    public ResponseEntity<BookingDto> save(@RequestBody CreateBookingRequest request) throws HikerAgeIsNotAppropriateException, TrailNotFoundException {
        BookingDto dto = bookingService.save(request);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<BookingDto> getBookingById(@PathVariable("id") Long id) throws BookingNotFoundException {
        BookingDto dto = bookingService.getBookingDtoById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    
    @PostMapping("/cancel/{id}")
    public ResponseEntity<BookingDto> cancelBookingById(@PathVariable("id") Long id) throws BookingNotFoundException {
        BookingDto dto = bookingService.cancelBookingById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
