package com.maygul.booking.api.request;

import com.maygul.booking.service.dto.HikerDto;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateBookingRequest {
    
    @ApiModelProperty(required = true)
    private LocalDate hikingDate;
    
    @ApiModelProperty(required = true)
    private Long trailId;
    
    @ApiModelProperty(required = true)
    private List<HikerDto> hikers;
}
