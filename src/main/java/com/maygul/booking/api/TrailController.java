package com.maygul.booking.api;

import com.maygul.booking.exception.TrailNotFoundException;
import com.maygul.booking.persistence.specification.criteria.TrailSearchCriteria;
import com.maygul.booking.service.TrailService;
import com.maygul.booking.service.dto.SearchPageTrailDto;
import com.maygul.booking.service.dto.TrailDto;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("Trail Controller")
@RestController
@RequestMapping("/api/trail")
@AllArgsConstructor
public class TrailController {
    
    private TrailService trailService;
    
    @GetMapping("/search")
    public ResponseEntity<SearchPageTrailDto> search(@RequestParam(name = "pageNumber", defaultValue = "0", required = false) Integer pageNumber,
                                                     @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                     @RequestParam(name = "name", required = false) String name) {
        TrailSearchCriteria criteria = new TrailSearchCriteria(name);
        SearchPageTrailDto searchDto = trailService.search(pageNumber, pageSize, criteria);
        return new ResponseEntity<>(searchDto, HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<TrailDto> getTrailById(@PathVariable("id") Long id) throws TrailNotFoundException {
        TrailDto dto = trailService.getTrailDtoById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
