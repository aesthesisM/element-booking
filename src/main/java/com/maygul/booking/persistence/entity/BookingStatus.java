package com.maygul.booking.persistence.entity;

public enum BookingStatus {
    APPROVED, CANCELED
}
