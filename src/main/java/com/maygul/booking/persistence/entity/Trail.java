package com.maygul.booking.persistence.entity;

import com.maygul.booking.common.persistence.entity.BaseEntity;
import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_TRAIL")
public class Trail extends BaseEntity {
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "START_AT")
    private LocalTime startAt;
    
    @Column(name = "END_AT")
    private LocalTime endAt;
    
    @Column(name = "MINIMUM_AGE")
    private Long minimumAge;
    
    @Column(name = "MAXIMUM_AGE")
    private Long maximumAge;
    
    @Column(name = "UNIT_PRICE")
    private Double unitPrice;
    
    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }
    
}
