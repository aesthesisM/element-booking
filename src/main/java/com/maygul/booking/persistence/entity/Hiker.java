package com.maygul.booking.persistence.entity;

import com.maygul.booking.common.persistence.entity.BaseEntity;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_HIKER")
public class Hiker extends BaseEntity {
    
    @Column(name = "USER_NAME")
    private String username;
    
    @Column(name = "AGE")
    private Long age;
    
    @ManyToOne
    @JoinColumn(name = "BOOKING_ID", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "BOOKING_ID_TO_BOOKING_USER_DETAIL"))
    private Booking booking;
    
    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }
    
}
