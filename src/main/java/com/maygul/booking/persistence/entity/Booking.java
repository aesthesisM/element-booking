package com.maygul.booking.persistence.entity;

import com.maygul.booking.common.persistence.entity.BaseEntity;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "default_gen", sequenceName = "SEQ_BOOKING")
public class Booking extends BaseEntity {
    
    @Column(name = "HIKING_DATE")
    private LocalDate hikingDate;
    
    @OneToOne
    @JoinColumn(name = "HIKING_ID", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "HIKING_ID_TO_BOOKING"))
    private Trail trail;
    
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private BookingStatus status;
    
    @OneToMany(
        fetch = FetchType.LAZY,
        mappedBy = "booking"
    )
    List<Hiker> hikers = new ArrayList<>();
    
    @Column(name = "TOTAL_COST_FOR_HIKERS")
    private Double totalCostForHikers;
    
    @PrePersist
    public void prePersist() {
        setCreateDate(LocalDateTime.now());
    }
    
    @PreUpdate
    public void preUpdate() {
        setUpdateDate(LocalDateTime.now());
    }
}
