package com.maygul.booking.persistence.specification.criteria;

import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class BookingSearchCriteria {
    private Long trailId;
    private LocalDate hikingDate;
    private LocalDateTime beginDate;
    private LocalDateTime endDate;
}
