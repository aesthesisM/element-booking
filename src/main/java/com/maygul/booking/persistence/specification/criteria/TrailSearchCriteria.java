package com.maygul.booking.persistence.specification.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TrailSearchCriteria {
    private String name;
}
