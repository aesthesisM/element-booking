package com.maygul.booking.persistence.specification;

import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.persistence.specification.criteria.TrailSearchCriteria;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public class TrailSpecification {
    
    public static Specification<Trail> findBySearchCriteria(TrailSearchCriteria criteria) {
        return (root, cq, cb) -> {
            List<Predicate> predicateList = new ArrayList<>();
            
            if (criteria.getName() != null) {
                Predicate namePredicate = cb.equal(cb.lower(root.get("name")), criteria.getName().toLowerCase());
                predicateList.add(namePredicate);
            }
            return cb.and(predicateList.toArray(new Predicate[] {}));
        };
    }
}
