package com.maygul.booking.persistence.specification;

import com.maygul.booking.persistence.entity.Booking;
import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.persistence.specification.criteria.BookingSearchCriteria;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public class BookingSpecification {
    
    public static Specification<Booking> findBySearchCriteria(BookingSearchCriteria criteria) {
        return (root, cq, cb) -> {
            List<Predicate> predicateList = new ArrayList<>();
            
            if (criteria.getTrailId() != null) {
                Join<Booking, Trail> trailJoin = root.join("trail");
                Predicate hikingPredicate = cb.equal(trailJoin.get("id"), criteria.getTrailId());
                predicateList.add(hikingPredicate);
            }
            
            if (criteria.getHikingDate() != null) {
                Predicate hikingDatePredicate = cb.equal(root.get("hikingDate"), criteria.getHikingDate());
                predicateList.add(hikingDatePredicate);
            }
            
            if (criteria.getBeginDate() != null) {
                Predicate beginDatePredicate = cb.greaterThanOrEqualTo(root.get("createDate"), criteria.getBeginDate());
                predicateList.add(beginDatePredicate);
            }
            
            if (criteria.getEndDate() != null) {
                Predicate endDatePredicate = cb.lessThanOrEqualTo(root.get("createDate"), criteria.getEndDate());
                predicateList.add(endDatePredicate);
            }
            
            return cb.and(predicateList.toArray(new Predicate[] {}));
        };
    }
}
