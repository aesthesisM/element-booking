package com.maygul.booking.persistence.repository;

import com.maygul.booking.persistence.entity.Hiker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HikerRepository extends JpaRepository<Hiker, Long> {
}
