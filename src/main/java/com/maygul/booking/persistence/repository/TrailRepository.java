package com.maygul.booking.persistence.repository;

import com.maygul.booking.persistence.entity.Trail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TrailRepository extends JpaRepository<Trail, Long>, JpaSpecificationExecutor<Trail> {
}
