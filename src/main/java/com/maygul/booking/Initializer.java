package com.maygul.booking;

import com.maygul.booking.persistence.entity.Trail;
import com.maygul.booking.persistence.repository.TrailRepository;
import java.time.LocalTime;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class Initializer implements CommandLineRunner {
    
    private TrailRepository trailRepository;
    
    @Override
    public void run(String... args) throws Exception {
        
        List<Trail> trails = trailRepository.findAll();
        if (trails.size() == 0) {
            Trail shire = new Trail();
            shire.setName("Shire");
            shire.setStartAt(LocalTime.of(7, 0));
            shire.setEndAt(LocalTime.of(9, 0));
            shire.setMinimumAge(5l);
            shire.setMaximumAge(100l);
            shire.setUnitPrice(29.90d);
            
            Trail gondor = new Trail();
            gondor.setName("Gondor");
            gondor.setStartAt(LocalTime.of(10, 0));
            gondor.setEndAt(LocalTime.of(13, 0));
            gondor.setMinimumAge(11l);
            gondor.setMaximumAge(50l);
            gondor.setUnitPrice(59.90d);
            
            Trail mordor = new Trail();
            mordor.setName("Mordor");
            mordor.setStartAt(LocalTime.of(14, 0));
            mordor.setEndAt(LocalTime.of(19, 0));
            mordor.setMinimumAge(18l);
            mordor.setMaximumAge(40l);
            mordor.setUnitPrice(99.90);
            
            
            trailRepository.save(shire);
            trailRepository.save(gondor);
            trailRepository.save(mordor);
        }
    }
}
