package booking.api;

import static org.junit.jupiter.api.Assertions.assertTrue;


import booking.AbstractSpringRunner;
import com.maygul.booking.api.request.CreateBookingRequest;
import com.maygul.booking.service.dto.BookingDto;
import com.maygul.booking.service.dto.HikerDto;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

class BookingControllerTest extends AbstractSpringRunner {
    
    private TestRestTemplate restTemplate;
    private HttpHeaders headers;
    
    @BeforeEach
    public void init() {
        restTemplate = new TestRestTemplate();
        headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    }
    
    
    @Test
    void saveGivenTrail_thenCheckResponseIfTrailIsSaved() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(super.createInitialUrlWithPort("/api/booking"));
        
        CreateBookingRequest request = new CreateBookingRequest();
        request.setTrailId(1l);
        request.setHikingDate(LocalDate.now());
        List<HikerDto> hikers = Arrays.asList(new HikerDto("andy blueman", 42l), new HikerDto("ahmed romel", 43l), new HikerDto("ronny k.", 44l));
        request.setHikers(hikers);
        
        HttpEntity<CreateBookingRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<BookingDto> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, entity, BookingDto.class);
        
        assertTrue(response.getBody().getId() != null, "Trail could not saved");
    }
    
    @Test
    void sendUnappropriateBooking_thenCheckResponseStatus_andConfirmInternalServerError() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(super.createInitialUrlWithPort("/api/booking"));
        
        CreateBookingRequest request = new CreateBookingRequest();
        request.setTrailId(1l);
        request.setHikingDate(LocalDate.now());
        List<HikerDto> hikers = Arrays.asList(new HikerDto("andy blueman", 4l), new HikerDto("ahmed romel", 101l), new HikerDto("ronny k.", 44l));
        request.setHikers(hikers);
        
        HttpEntity<CreateBookingRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<BookingDto> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, entity, BookingDto.class);
        assertTrue(response.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR, "Booking should not be saved");
    }
    
}
