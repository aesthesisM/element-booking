INSTALLATION

clone the application then run the following maven command to build project

bash ./mvnw clean install -P dev

then build docker image with following docker command

docker build -t element-booking:v1.0.0 .

also pull postgres:latest image from dockerhub with following command

docker pull postgres:latest

after building image run the following docker-compose command to wake postgresql and booking image together

docker-compose up

After application run successfully, you can reach swagger via http://localhost:8080/swagger-ui.html to test endpoints.

For production readiness, Response classes would be written. Other api method, service and repository tests should be written. Swagger API should be disabled... Loggin and Authentication should be implemented. And for database it's data would be mapped as volume